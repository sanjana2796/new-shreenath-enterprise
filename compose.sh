#!/bin/bash
#   CSS
#echo -e "\nCopying css to dist"
#mkdir -p 'dist/' && cp -R html/css dist/css
#echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   JS
#echo -e "\nCopying js to dist"
#mkdir -p 'dist/' && cp -R html/js dist/js
#echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   Revolution js
echo -e "\nCopying revolution js to dist"
mkdir -p 'dist/' && cp -R html/revolution/js dist/revolution_js
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   Revolution css
echo -e "\nCopying revolution css to dist"
mkdir -p 'dist/' && cp -R html/revolution/js dist/revolution_css
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   fonts
echo -e "\nCopying fonts to dist"
mkdir -p 'dist/' && cp -R html/fonts dist/fonts
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
#   Templates
echo -e "\nComposing templates"
echo -e "`date "+%Y-%m-%d %H:%M:%S"`"
hbs src/index.hbs --partial 'src/partials/*.hbs' --output dist/home/ && hbs src/about-us/index.hbs --partial 'src/partials/*.hbs' --output dist/about/ && hbs src/Services/index.hbs --partial 'src/partials/*.hbs' --output dist/services/ && hbs src/Portfolio/index.hbs --partial 'src/partials/*.hbs' --output dist/portfolio/ && hbs src/contact-us/index.hbs --partial 'src/partials/*.hbs' --output dist/contact/ && exit 1;